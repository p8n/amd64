/*
 * Panopticon - A libre disassembler
 * Copyright (C) 2014,2015,2016  Panopticon authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//! RREIL code generator for Intel x86 and AMD64.
//!
//! This module defines a function for each Intel mnemonic recognized by Panopticon. This function
//! returns RREIL statements implementing the opcode semantics and a `JumpSpec` instance
//! that tells the disassembler where to continue.
//!
//! The RREIL code can be generated using the `rreil!` macro. It returns `Result<Vec<Statement>>`.
//!
//! This modules has some helper functions to make things easier (and results more correct) for the
//! casual contributor. For setting various arithmetic flags use the `set_*_flag` functions. Assign
//! register values using `write_reg`. This makes sure that e.g. EAX, AX, AH and AL are written
//! when RAX is. Also, remember to sign or zero extend input Value instance using `sign_extend`/`zero_extend`. RREIL
//! does not extend values automatically.
//!
//! RREIL has no traps, software interrupts of CPU exceptions, this part of the Intel CPUs can be
//! ignored for now. Also, no paging or segmentation is implemented. Memory addresses are used
//! as-is.
//!
//! When implementing opcodes the instruction set reference in volume 2 of the Intel Software
//! Developer's Manual should be the primary source of inspiration ;-). Aside from that other
//! (RREIL) code generator are worth a look e.g.
//!
//!  * https://github.com/Cr4sh/openreil
//!  * https://github.com/StanfordPL/stoke
//!  * https://github.com/snf/synthir
//!  * https://github.com/c01db33f/reil
//!
//! Simple opcodes that do not require memory access and/or jump/branch can be verified against
//! the CPU directly using a `QuickCheck` harness that's part of the Panopticon test suite. See
//! `tests/amd64.rs` for how to use it.
//!
//! In case you add opcode semantics please update issue #36.

#![allow(dead_code)]

use disassembler::{Condition, JumpSpec};

use p8n_types::{
    Guard,
    Variable,
    Constant,
    Result,
    Value,
    Statement
};
use p8n_rreil_macro::rreil;

use std::cmp::max;

/// Sets the adjust flag AF after an addition. Assumes res := a + ?.
fn set_adj_flag(res: &Variable, a: &Value) -> Result<Vec<Statement>> {
    rreil!{
        mov nibble_res:4, (res)
        mov nibble_a:4, (a)
        cmpltu AF:1, nibble_res:4, nibble_a:4
    }
}

/// Sets the adjust flag AF after a subtraction. Assumes res := a - ?.
fn set_sub_adj_flag(res: &Variable, a: &Value) -> Result<Vec<Statement>> {
    rreil!{
        mov nibble_res:4, (res)
        mov nibble_a:4, (a)
        cmpltu AF:1, nibble_a:4, nibble_res:4
    }
}

/// Sets the parity flag PF.
fn set_parity_flag(res: &Variable) -> Result<Vec<Statement>> {
    rreil!{
        mov half_res:8, res:8
        mov PF:1, half_res:1
        sel/1/1 b:1, half_res:8
        xor PF:1, PF:1, b:1
        sel/2/1 b:1, half_res:8
        xor PF:1, PF:1, b:1
        sel/3/1 b:1, half_res:8
        xor PF:1, PF:1, b:1
        sel/4/1 b:1, half_res:8
        xor PF:1, PF:1, b:1
        sel/5/1 b:1, half_res:8
        xor PF:1, PF:1, b:1
        sel/6/1 b:1, half_res:8
        xor PF:1, PF:1, b:1
        sel/7/1 b:1, half_res:8
        xor PF:1, PF:1, b:1
        xor PF:1, PF:1, [1]:1
    }
}

/// Sets the carry flag CF after an addition. Assumes res := a + ?.
fn set_carry_flag(res: &Variable, a: &Value) -> Result<Vec<Statement>> {
    rreil!{
        cmpeq cf1:1, (res), (a)
        cmpltu cf2:1, (res), (a)
        //and cf1:1, cf1:1, CF:1
        or CF:1, cf1:1, cf2:1
    }
}

/// Sets the carry flag CF after a subtraction. Assumes res := a + ?.
fn set_sub_carry_flag(res: &Variable, a: &Value) -> Result<Vec<Statement>> {
    rreil!{
        cmpeq cf1:1, (res), (a)
        cmpltu cf2:1, (a), (res)
        //and cf1:1, cf1:1, CF:1
        or CF:1, cf1:1, cf2:1
    }
}

/// Sets the overflow flag OF. Assumes res := a ? b.
fn set_overflow_flag(res: &Variable, a: &Value, b: &Value, sz: u16) -> Result<Vec<Statement>> {
    /*
     * The rules for turning on the overflow flag in binary/integer math are two:
     *
     * 1. If the sum of two numbers with the sign bits off yields a result number
     *    with the sign bit on, the "overflow" flag is turned on.
     *
     *    0100 + 0100 = 1000 (overflow flag is turned on)
     *
     * 2. If the sum of two numbers with the sign bits on yields a result number
     *    with the sign bit off, the "overflow" flag is turned on.
     *
     *    1000 + 1000 = 0000 (overflow flag is turned on)
     *
     * Otherwise, the overflow flag is turned off.
     */
    let msb = sz - 1;
    rreil!{
        xor of1:sz, (a), (b)
        xor of1:sz, of1:sz, [0xffffffffffffffff]:sz
        xor of2:sz, (a), (res)
        sel/msb/1 a1:1, of1:sz
        sel/msb/1 a2:1, of2:sz
        and OF:1, a1:1, a2:1
    }
}

/// Assumes res := a ? b
fn set_sub_overflow_flag(res: &Variable, a: &Value, b: &Value, sz: u16) -> Result<Vec<Statement>> {
    /*
     * The rules for turning on the overflow flag in binary/integer math are two:
     *
     * 1. If the sum of two numbers with the sign bits off yields a result number
     *    with the sign bit on, the "overflow" flag is turned on.
     *
     *    0100 + 0100 = 1000 (overflow flag is turned on)
     *
     * 2. If the sum of two numbers with the sign bits on yields a result number
     *    with the sign bit off, the "overflow" flag is turned on.
     *
     *    1000 + 1000 = 0000 (overflow flag is turned on)
     *
     * Otherwise, the overflow flag is turned off.
     */
    let msb = sz - 1;
    rreil!{
        xor of1:sz, (a), (b)
        xor of2:sz, (b), (res)
        xor of2:sz, of2:sz, [0xffffffffffffffff]:sz
        sel/msb/1 a1:1, of1:sz
        sel/msb/1 a2:1, of2:sz
        and OF:1, a1:1, a2:1
    }
}

/// Sign extends `a` and `b` to `max(a.size,b.size)`.
fn sign_extend(a: &Value, b: &Value) -> Result<(Value, Value, u16, Vec<Statement>)> {
    extend(a, b, true)
}

fn zero_extend(a: &Value, b: &Value) -> Result<(Value, Value, u16, Vec<Statement>)> {
    extend(a, b, false)
}

/// Returns (a/sz, b/sz, sz) w/ s = max(a.size,b.size)
fn extend(a: &Value, b: &Value, sign_ext: bool) -> Result<(Value, Value, u16, Vec<Statement>)> {
    let sz = max(a.bits().unwrap_or(0), b.bits().unwrap_or(0));
    let mut ext = |x: &Value, s: u16| -> Result<Value> {
        match x.clone() {
            Value::Undefined => Ok(Value::Undefined),
            Value::Variable(Variable{ name, bits }) => {
                if bits != s {
                    Ok(Value::Variable(Variable::new2(format!("{}_ext", name.base()), None, s)?))
                } else {
                    Ok(x.clone())
                }
            }
            Value::Constant(Constant{ value, bits }) => {
                if bits != s {
                    Ok(Value::val(value,s)?)
                } else {
                    Ok(x.clone())
                }
            }
        }
    };

    let ext_a = ext(a, sz)?;
    let ext_b = ext(b, sz)?;
    let mut stmts = vec![];

    assert!(sz > 0);
    assert!(ext_a.bits() == None || ext_b.bits() == None || ext_a.bits() == ext_b.bits());

    if a.bits() != ext_a.bits() {
        match ext_a.clone() {
            Value::Variable(lv) => {
                if sign_ext {
                    stmts = rreil!{ sext/sz (lv), (a) }?;
                } else {
                    stmts = rreil!{ zext/sz (lv), (a) }?;
                }
            }
            _ => {}
        }
    }

    if b.bits() != ext_b.bits() {
        match ext_b.clone() {
            Value::Variable(lv) => {
                if sign_ext {
                    stmts.append(&mut rreil!{ sext/sz (lv), (a) }?);
                } else {
                    stmts.append(&mut rreil!{ zext/sz (lv), (a) }?);
                }
            }
            _ => {}
        }
    }

    Ok((ext_a, ext_b, sz, stmts))
}

/// Returns all sub- and super registers for `name` or None if `name` isn't a register.
fn reg_variants(name: &str) -> Result<(Option<Variable>, Option<Variable>, Option<Variable>, Option<Variable>, Option<Variable>)> {
    match name {
        "AL" | "AH" | "AX" | "EAX" | "RAX" => {
            let reg8l = Some(Variable::new2("AL",None,8)?);
            let reg8h = Some(Variable::new2("AH",None,8)?);
            let reg16 = Some(Variable::new2("AX",None,16)?);
            let reg32 = Some(Variable::new2("EAX",None,32)?);
            let reg64 = Some(Variable::new2("RAX",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "BL" | "BH" | "BX" | "EBX" | "RBX" => {
            let reg8l = Some(Variable::new2("BL",None,8)?);
            let reg8h = Some(Variable::new2("BH",None,8)?);
            let reg16 = Some(Variable::new2("BX",None,16)?);
            let reg32 = Some(Variable::new2("EBX",None,32)?);
            let reg64 = Some(Variable::new2("RBX",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "CL" | "CH" | "CX" | "ECX" | "RCX" => {
            let reg8l = Some(Variable::new2("CL",None,8)?);
            let reg8h = Some(Variable::new2("CH",None,8)?);
            let reg16 = Some(Variable::new2("CX",None,16)?);
            let reg32 = Some(Variable::new2("ECX",None,32)?);
            let reg64 = Some(Variable::new2("RCX",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "DL" | "DH" | "DX" | "EDX" | "RDX" => {
            let reg8l = Some(Variable::new2("DL",None,8)?);
            let reg8h = Some(Variable::new2("DH",None,8)?);
            let reg16 = Some(Variable::new2("DX",None,16)?);
            let reg32 = Some(Variable::new2("EDX",None,32)?);
            let reg64 = Some(Variable::new2("RDX",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "SIL" | "SIH" | "SI" | "ESI" | "RSI" => {
            let reg8l = Some(Variable::new2("SIL",None,8)?);
            let reg8h = Some(Variable::new2("SIH",None,8)?);
            let reg16 = Some(Variable::new2("SI",None,16)?);
            let reg32 = Some(Variable::new2("ESI",None,32)?);
            let reg64 = Some(Variable::new2("RSI",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "DIL" | "DIH" | "DI" | "EDI" | "RDI" => {
            let reg8l = Some(Variable::new2("DIL",None,8)?);
            let reg8h = Some(Variable::new2("DIH",None,8)?);
            let reg16 = Some(Variable::new2("DI",None,16)?);
            let reg32 = Some(Variable::new2("EDI",None,32)?);
            let reg64 = Some(Variable::new2("RDI",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "BPL" | "BP" | "EBP" | "RBP" => {
            let reg8l = Some(Variable::new2("BPL",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("BP",None,16)?);
            let reg32 = Some(Variable::new2("EBP",None,32)?);
            let reg64 = Some(Variable::new2("RBP",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "SPL" | "SP" | "ESP" | "RSP" => {
            let reg8l = Some(Variable::new2("SPL",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("SP",None,16)?);
            let reg32 = Some(Variable::new2("ESP",None,32)?);
            let reg64 = Some(Variable::new2("RSP",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "IP" | "EIP" | "RIP" => {
            let reg8l = None;
            let reg8h = None;
            let reg16 = Some(Variable::new2("IP",None,16)?);
            let reg32 = Some(Variable::new2("EIP",None,32)?);
            let reg64 = Some(Variable::new2("RIP",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "R8B" | "R8W" | "R8D" | "R8" => {
            let reg8l = Some(Variable::new2("R8B",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("R8W",None,16)?);
            let reg32 = Some(Variable::new2("R8D",None,32)?);
            let reg64 = Some(Variable::new2("R8",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "R9B" | "R9W" | "R9D" | "R9" => {
            let reg8l = Some(Variable::new2("R9B",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("R9W",None,16)?);
            let reg32 = Some(Variable::new2("R9D",None,32)?);
            let reg64 = Some(Variable::new2("R9",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "R10B" | "R10W" | "R10D" | "R10" => {
            let reg8l = Some(Variable::new2("R10B",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("R10W",None,16)?);
            let reg32 = Some(Variable::new2("R10D",None,32)?);
            let reg64 = Some(Variable::new2("R10",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "R11B" | "R11W" | "R11D" | "R11" => {
            let reg8l = Some(Variable::new2("R11B",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("R11W",None,16)?);
            let reg32 = Some(Variable::new2("R11D",None,32)?);
            let reg64 = Some(Variable::new2("R11",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "R12B" | "R12W" | "R12D" | "R12" => {
            let reg8l = Some(Variable::new2("R12B",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("R12W",None,16)?);
            let reg32 = Some(Variable::new2("R12D",None,32)?);
            let reg64 = Some(Variable::new2("R12",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "R13B" | "R13W" | "R13D" | "R13" => {
            let reg8l = Some(Variable::new2("R13B",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("R13W",None,16)?);
            let reg32 = Some(Variable::new2("R13D",None,32)?);
            let reg64 = Some(Variable::new2("R13",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "R14B" | "R14W" | "R14D" | "R14" => {
            let reg8l = Some(Variable::new2("R14B",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("R14W",None,16)?);
            let reg32 = Some(Variable::new2("R14D",None,32)?);
            let reg64 = Some(Variable::new2("R14",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        "R15B" | "R15W" | "R15D" | "R15" => {
            let reg8l = Some(Variable::new2("R15B",None,8)?);
            let reg8h = None;
            let reg16 = Some(Variable::new2("R15W",None,16)?);
            let reg32 = Some(Variable::new2("R15D",None,32)?);
            let reg64 = Some(Variable::new2("R15",None,64)?);

            Ok((reg8l,reg8h,reg16,reg32,reg64))
        }
        _ => Ok((None,None,None,None,None))
    }
}

/// Assigns `val:sz` to `reg`. This function makes sure all that e.g. EAX is written when RAX is.
fn write_reg(reg: &Value, val: &Value, sz: u16) -> Result<Vec<Statement>> {
    use std::cmp;
    use std::num::Wrapping;

    match reg.clone() {
        Value::Variable(Variable{ name, bits }) => {
            let mut hi = sz;
            let mut lo = 0;
            let mut stmts;
            let (reg8l, reg8h, reg16, reg32, reg64) = reg_variants(name.base())?;

            if reg8h.is_some() && *reg == reg8h.clone().unwrap().into() {
                hi += 8;
                lo += 8;
            }

            if lo == 0 && hi == 64 && val.bits() == Some(64) {
                stmts = rreil!{
                    mov val:64, (val)
                }?;
            } else {
                stmts = rreil!{
                    zext/64 val:64, (val)
                }?;
            }

            if lo > 0 {
                let shft = 1 << lo;
                stmts.append(&mut rreil!{
                        mul val:64, val:64, [shft]:64
                }?);
            }

            // *L
            if lo <= 7 {
                let msk = !((0xff << lo) % (1 << cmp::min(8, hi))) & 0xff;

                if let Some(reg8l) = reg8l.clone() {
                    if msk == 0 {
                        stmts.append(&mut rreil!{
                                mov (reg8l), val:8
                        }?);
                    } else if msk < 0xff {
                        stmts.append(&mut rreil!{
                                and (reg8l), (reg8l), [msk]:8
                                or (reg8l), (reg8l), val:8
                        }?);
                    }
                }
            }

            // *H
            if hi >= 9 && lo <= 15 {
                if let Some(reg8h) = reg8h.clone() {
                    let msk = (!((0xffff << lo) % (1u64 << cmp::min(16, hi))) & 0xffff) >> 8;
                    if msk == 0 {
                        stmts.append(&mut rreil!{
                                sel/8/8 (reg8h), val:16
                        }?);
                    } else if msk < 0xff {
                        stmts.append(&mut rreil!{
                                and (reg8h), (reg8h), [msk]:8
                                sel/8/8 hibyte:8, val:16
                                or (reg8h), (reg8h), hibyte:8
                        }?);
                    }
                }
            }

            // *X
            if lo <= 15 {
                if let Some(reg16) = reg16.clone() {
                    let msk = !((0xffff << lo) % (1u64 << cmp::min(16, hi))) & 0xffff;

                    if msk == 0 {
                        stmts.append(&mut rreil!{
                                mov (reg16), val:16
                        }?);
                    } else if msk < 0xffff {
                        stmts.append(&mut rreil!{
                                and (reg16), (reg16), [msk]:16
                                or (reg16), (reg16), val:16
                        }?);
                    }
                }
            }

            // E*X
            if lo <= 31 {
                if let Some(reg32) = reg32.clone() {
                    let msk = !((0xffffffff << lo) % (1u64 << cmp::min(32, hi))) & 0xffffffff;

                    if msk == 0 {
                        stmts.append(&mut rreil!{
                                mov (reg32), val:32
                        }?);
                    } else if msk < 0xffffffff {
                        stmts.append(&mut rreil!{
                                and (reg32), (reg32), [msk]:32
                                or (reg32), (reg32), val:32
                        }?);
                    }
                }
            }

            // R*X
            if lo <= 64 {
                if let Some(reg64) = reg64.clone() {
                    let msk = if hi < 64 {
                        !((Wrapping(0xffffffffffffffffu64) << lo).0 % (1 << hi))
                    } else {
                        !(Wrapping(0xffffffffffffffffu64) << lo).0
                    };

                    if msk == 0 || (lo == 0 && hi == 32) {
                        stmts.append(&mut rreil!{
                                mov (reg64), val:64
                        }?);
                    } else if msk < 0xffffffffffffffff {
                        stmts.append(&mut rreil!{
                                and (reg64), (reg64), [msk]:64
                                or (reg64), (reg64), val:64
                        }?);
                    }
                }
            }

            if reg8l.is_none() && reg8h.is_none() && reg16.is_none() && reg32.is_none() && reg64.is_none() {
                let lv = Variable::new(name,bits)?;
                stmts = rreil!{
                    mov (lv),(val)
                }?;
            }

            Ok(stmts)
        }
        _ => {
            Err(format!("Internal error: called write_reg with {:?}", reg).into())
        }
    }
}

pub fn aaa() -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    /*  rreil!{
        and y:8, AL:8, [0xf]:8;

        // TODO
    }

    let y = new_temp(16);
    let x1 = new_temp(1);
    let x2 = new_temp(1);

    cg.and_b(&y,&*AL,&Value::Constant(0x0f));

    // x1 = !(y <= 9) || AF
    cg.equal_i(&x1,&y.clone().into(),&Value::Constant(9));
    cg.less_i(&x2,&y.clone().into(),&Value::Constant(9));
    cg.or_b(&x1,&x1.clone().into(),&x2.clone().into());
    cg.not_b(&x1,&x1.clone().into());
    cg.or_b(&x1,&x1.clone().into(),&AF.clone().into());

    cg.assign(&*AF,&x1.clone().into());
    cg.assign(&*CF,&x1.clone().into());

    // AX = (AX + x1 * 0x106) % 0x100
    cg.lift_b(&y,&x1.clone().into());
    cg.mul_i(&y,&y.clone().into(),&Value::Constant(0x106));
    cg.add_i(&AX,&AX.clone().into(),&y.clone().into());
    cg.mod_i(&AX,&AX.clone().into(),&Value::Constant(0x100));*/
}

pub fn aam(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    /*   let temp_al = new_temp(16);

    cg.assign(&temp_al,&AL.clone().into());
    cg.div_i(&*AH,&temp_al,&a);
    cg.mod_i(&*AL,&temp_al,&a);*/
}

pub fn aad(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    /*   let x = new_temp(16);

    cg.mul_i(&x,&AH.clone().into(),&a);
    cg.add_i(&*AL,&x,&AL.clone().into());
    cg.assign(&*AH,&Value::new_bit(0));*/
}

pub fn aas() -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    /*   let y1 = new_temp(16);
    let x1 = new_temp(1);
    let x2 = new_temp(1);

    cg.and_b(&y1,&*AL,&Value::Constant(0x0f));

    // x1 = !(y <= 9) || AF
    cg.equal_i(&x1,&y1.clone().into(),&Value::Constant(9));
    cg.less_i(&x2,&y1.clone().into(),&Value::Constant(9));
    cg.or_b(&x1,&x1.clone().into(),&x2.clone().into());
    cg.not_b(&x1,&x1.clone().into());
    cg.or_b(&x1,&x1.clone().into(),&AF.clone().into());

    cg.assign(&*AF,&x1.clone().into());
    cg.assign(&*CF,&x1.clone().into());

    let y2 = new_temp(16);

    // AX = (AX - x1 * 6) % 0x100
    cg.lift_b(&y2,&x1.clone().into());
    cg.mul_i(&y2,&y2.clone().into(),&Value::Constant(6));
    cg.sub_i(&AX,&AX.clone().into(),&y2.clone().into());
    cg.mod_i(&AX,&AX.clone().into(),&Value::Constant(0x100));

    let z = new_temp(16);

    // AH = (AH - x1) % 0x10
    cg.lift_b(&z,&x1.clone().into());
    cg.sub_i(&AH,&AH.clone().into(),&z.clone().into());
    cg.mod_i(&AH,&AH.clone().into(),&Value::Constant(0x10));

    cg.assign(&*AL,&y1.clone().into());*/
}

pub fn adc(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (a, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    let res = Variable::new2("res", None, sz)?;

    stmts.append(&mut rreil!{
        add res:sz, (a), (b)
        zext/sz cf:sz, CF:1
        add res:sz, res:sz, cf:sz
        cmplts SF:1, res:sz, [0]:sz
        cmpeq ZF:1, res:sz, [0]:sz

        mov a:4, (a)
        cmpeq af1:1, res:4, a:4
        cmpltu af2:1, res:4, a:4
        and af1:1, af1:1, CF:1
        or AF:1, af1:1, af2:1
    }?);
    stmts.append(&mut set_carry_flag(&res, &a)?);
    stmts.append(&mut set_overflow_flag(&res, &a, &b, sz)?);
    stmts.append(&mut set_parity_flag(&res)?);
    stmts.append(&mut write_reg(&a_, &res.clone().into(), sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn add(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    // TODO: use this stmts or below? this is worrisome...
    let (a, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    let res = Variable::new2("res", None, sz)?;
    //let mut stmts = vec![];

    stmts.append(
        &mut rreil!{
        add res:sz, (a), (b)
        cmplts SF:1, res:sz, [0]:sz
        cmpeq ZF:1, res:sz, [0]:sz
        cmpltu CF:1, (res), (a)
    }?
    );
    stmts.append(&mut set_adj_flag(&res, &a)?);
    stmts.append(&mut set_overflow_flag(&res, &a, &b, sz)?);
    stmts.append(&mut set_parity_flag(&res)?);
    stmts.append(&mut write_reg(&a_, &res.clone().into(), sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn adcx(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (a, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    let res = Variable::new2("res", None, sz)?;

    stmts.append(
        &mut rreil!{
        add res:sz, (a), (b)
        zext/sz cf:sz, CF:1
        add res:sz, res:sz, cf:sz
    }?
    );
    stmts.append(&mut set_carry_flag(&res, &a)?);
    stmts.append(&mut write_reg(&a_, &res.clone().into(), sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn and(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (a, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    let res = Variable::new2("res", None, sz)?;

    stmts.append(
        &mut rreil!{
        and res:sz, (a), (b)
        cmplts SF:1, res:sz, [0]:sz
        cmpeq ZF:1, res:sz, [0]:sz
        mov CF:1, [0]:1
        mov OF:1, [0]:1
        mov AF:1, ?
    }?
    );
    stmts.append(&mut set_parity_flag(&res)?);
    stmts.append(&mut write_reg(&a_, &res.clone().into(), sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn arpl(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn bound(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn bsf(_a: Value, _b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    // let (_,b,sz,_) = try!(sign_extend(&_a,&_b));
    // let res = Variable::new2("res", None, sz)?;
    // let mut stmts = try!(rreil!{
    //     cmpeq ZF:1, (b), [0]:sz;
    //     mov res:sz, ?;
    // });

    // stmts.append(&mut try!(write_reg(&_a,&res.clone().into(),sz)));
    // Ok((stmts,JumpSpec::FallThru))
}

pub fn bsr(_a: Value, _b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    // let (_,b,sz,_) = try!(sign_extend(&_a,&_b));
    // let res = Variable::new2("res", None, sz)?;
    // let mut stmts = try!(rreil!{
    //     cmpeq ZF:1, (b), [0]:sz;
    //     mov res:sz, ?;
    // });

    // stmts.append(&mut try!(write_reg(&_a,&res.clone().into(),sz)));
    // Ok((stmts,JumpSpec::FallThru))
}

pub fn bswap(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    //unimplemented!()
    /*
    using dsl::operator*;

    size_t const a_w = bitwidth(a);
    size_t byte = 0;

    rvalue tmp = undefined();

    while(byte < a_w / 8)
    {
        unsigned int lsb = byte * 8;
        unsigned int div = (1 << lsb), mul = (1 << (a_w - byte * 8));

        tmp = tmp + (((a / div) % Value::Constant(0x100)) * mul);
        ++byte;
    }

    m.assign(to_lvalue(a),tmp);*/





}

pub fn bt(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    //unimplemented!()
    /*
    using dsl::operator<<;
    rvalue mod = (Value::Constant(1) << (b % constant(bitwidth(a))));

    m.assign(to_lvalue(CF), (a / mod) % 2);
    m.assign(to_lvalue(PF), undefined());
    m.assign(to_lvalue(OF), undefined());
    m.assign(to_lvalue(SF), undefined());
    m.assign(to_lvalue(AF), undefined());*/





}

pub fn btc(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    //unimplemented!()
    /*
    using dsl::operator<<;
    rvalue mod = (Value::Constant(1) << (b % constant(bitwidth(a))));

    m.assign(to_lvalue(CF), (a / mod) % 2);
    m.assign(to_lvalue(PF), undefined());
    m.assign(to_lvalue(OF), undefined());
    m.assign(to_lvalue(SF), undefined());
    m.assign(to_lvalue(AF), undefined());
    m.assign(to_lvalue(a),a ^ mod);*/





}

pub fn btr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    //unimplemented!()
    /*
    using dsl::operator<<;
    size_t const a_w = bitwidth(a);
    rvalue mod =  ((Value::Constant(1) << (b % constant(bitwidth(a)))));

    m.assign(to_lvalue(CF), (a / mod) % 2);
    m.assign(to_lvalue(PF), undefined());
    m.assign(to_lvalue(OF), undefined());
    m.assign(to_lvalue(SF), undefined());
    m.assign(to_lvalue(AF), undefined());
    m.assign(to_lvalue(a),(a & (Value::Constant(0xffffffffffffffff) ^ mod)) % constant(1 << a_w));*/





}

pub fn bts(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    //unimplemented!()
    /*
    using dsl::operator<<;
    rvalue mod = (Value::Constant(1) << (b % constant(bitwidth(a))));

    m.assign(to_lvalue(CF), (a / mod) % 2);
    m.assign(to_lvalue(PF), undefined());
    m.assign(to_lvalue(OF), undefined());
    m.assign(to_lvalue(SF), undefined());
    m.assign(to_lvalue(AF), undefined());
    m.assign(to_lvalue(a),a & mod);*/





}

pub fn call(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let stmts = rreil!{
            zext/64 a:64, (a)
            call a:64
    }?;
    Ok((stmts, JumpSpec::FallThru))
}

pub fn cmovcc(_: Value, _: Value, _: Condition) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    /*   let a = Variable::from_rvalue(&_a).unwrap();
    let fun = |f: &Variable,| {
        let l = new_temp(bitwidth(&a.clone().into()));
        let nl = new_temp(bitwidth(&a.clone().into()));
        let n = new_temp(1);

        cg.lift_b(&l,&f.clone().into());
        cg.not_b(&n,&f.clone().into());
        cg.lift_b(&nl,&n);
        cg.mul_i(&l,&l,&b);
        cg.mul_i(&nl,&nl,&a.clone().into());
        cg.add_i(&a,&l,&nl);
    };

    match c {
        Condition::Overflow => fun(&*OF,cg),
        Condition::NotOverflow =>  {
            let nof = new_temp(1);
            cg.not_b(&nof,&OF.clone().into());
            fun(&nof,cg)
        },
        Condition::Carry => fun(&*CF,cg),
        Condition::AboveEqual => {
            let ncf = new_temp(1);
            cg.not_b(&ncf,&CF.clone().into());
            fun(&ncf,cg)
        },
        Condition::Equal => fun(&*ZF,cg),
        Condition::NotEqual => {
            let nzf = new_temp(1);
            cg.not_b(&nzf,&ZF.clone().into());
            fun(&nzf,cg)
        },
        Condition::BelowEqual => {
            let zc = new_temp(1);
            cg.or_b(&zc,&ZF.clone().into(),&CF.clone().into());
            fun(&zc,cg)
        },
        Condition::Above => {
            let zc = new_temp(1);
            cg.or_b(&zc,&ZF.clone().into(),&CF.clone().into());
            cg.not_b(&zc,&zc);
            fun(&zc,cg)
        },
        Condition::Sign => fun(&*SF,cg),
        Condition::NotSign => {
            let nsf = new_temp(1);
            cg.not_b(&nsf,&SF.clone().into());
            fun(&nsf,cg)
        },
        Condition::Parity => fun(&*PF,cg),
        Condition::NotParity => {
            let npf = new_temp(1);
            cg.not_b(&npf,&PF.clone().into());
            fun(&npf,cg)
        },
        Condition::Less => {
            let b = new_temp(1);
            cg.xor_b(&b,&SF.clone().into(),&OF.clone().into());
            cg.not_b(&b,&b.clone().into());
            fun(&b,cg)
        },
        Condition::GreaterEqual => {
            let b = new_temp(1);
            cg.xor_b(&b,&SF.clone().into(),&OF.clone().into());
            fun(&b,cg)
        },
        Condition::LessEqual => {
            let b = new_temp(1);
            cg.xor_b(&b,&SF.clone().into(),&OF.clone().into());
            cg.not_b(&b,&b.clone().into());
            cg.or_b(&b,&b,&ZF.clone().into());
            fun(&b,cg)
        },
        Condition::Greater => {
            let b = new_temp(1);
            let z = new_temp(1);
            cg.xor_b(&b,&SF.clone().into(),&OF.clone().into());
            cg.not_b(&z,&ZF.clone().into());
            cg.or_b(&b,&b,&z.clone().into());
            fun(&b,cg)
        },
    }*/
}

pub fn cmp(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (a, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    let res = Variable::new2("res", None, sz)?;

    stmts.append(
        &mut rreil!{
        sub res:sz, (a), (b)
        cmplts SF:1, res:sz, [0]:sz
        cmpeq ZF:1, res:sz, [0]:sz
    }?
    );
    stmts.append(&mut set_sub_carry_flag(&res, &a)?);
    stmts.append(&mut set_sub_adj_flag(&res, &a)?);
    stmts.append(&mut set_sub_overflow_flag(&res, &a, &b, sz)?);
    stmts.append(&mut set_parity_flag(&res)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn cmpsw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cmpsb() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

/*pub fn cmps(_: Value, _: Value) -> Result<(Vec<Statement>,JumpSpec)> {
    return Ok((vec![],JumpSpec::FallThru));
 /*   let a = Variable::Memory{
        offset: Box::new(aoff.clone()),
        bytes: 1,
        endianess: Endianess::Little,
        name: "ram".to_string()
    };
    let b = Variable::Memory{
        offset: Box::new(boff.clone()),
        bytes: 1,
        endianess: Endianess::Little,
        name: "ram".to_string()
    };
    let res = new_temp(8);
    let off = new_temp(bitwidth(&aoff));
    let n = new_temp(1);
    let df = new_temp(bitwidth(&aoff));
    let ndf = new_temp(bitwidth(&aoff));

    cg.sub_i(&res,&a.clone().into(),&b.clone().into());
    set_arithm_flags(&res,&res.clone().into(),&a.clone().into(),cg);

    cg.lift_b(&df,&DF.clone().into());
    cg.not_b(&n,&DF.clone().into());
    cg.lift_b(&ndf,&n.clone().into());

    cg.sub_i(&off,&df,&ndf);

    let ao = Variable::from_rvalue(&aoff).unwrap();
    let bo = Variable::from_rvalue(&boff).unwrap();
    cg.add_i(&ao,&aoff,&off);
    cg.add_i(&bo,&boff,&off);*/
}*/

pub fn cmpxchg(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    return Ok((vec![], JumpSpec::FallThru));
    /*   cg.equal_i(&*ZF,&a,&EAX.clone().into());

    let n = new_temp(1);
    let zf = new_temp(32);
    let nzf = new_temp(32);
    let la = Variable::from_rvalue(&a).unwrap();

    cg.lift_b(&zf,&ZF.clone().into());
    cg.not_b(&n,&ZF.clone().into());
    cg.lift_b(&nzf,&n.clone().into());
    cg.mul_i(&zf,&zf,&b);
    cg.mul_i(&nzf,&nzf,&a);
    cg.add_i(&la,&zf,&nzf);

    cg.lift_b(&zf,&ZF.clone().into());
    cg.lift_b(&nzf,&n.clone().into());
    cg.mul_i(&zf,&zf,&EAX.clone().into());
    cg.add_i(&*EAX,&zf,&nzf);*/
}

pub fn or(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (a, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    let res = Variable::new2("res", None, sz)?;

    stmts.append(
        &mut rreil!{
        or res:sz, (a), (b)
        cmplts SF:1, res:sz, [0]:sz
        cmpeq ZF:1, res:sz, [0]:sz
        mov CF:1, [0]:1
        mov OF:1, [0]:1
        mov AF:1, ?
    }?
    );
    stmts.append(&mut set_parity_flag(&res)?);
    stmts.append(&mut write_reg(&a_, &res.clone().into(), sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn sbb(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (a, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    let res = Variable::new2("res", None, sz)?;

    stmts.append(
        &mut rreil!{
        sub res:sz, (a), (b)
        zext/sz cf:sz, CF:1
        sub res:sz, res:sz, cf:sz
        cmplts SF:1, res:sz, [0]:sz
        cmpeq ZF:1, res:sz, [0]:sz

        mov a:4, (a)
        cmpeq af1:1, res:4, a:4
        cmpltu af2:1, a:4, res:4
        and af1:1, af1:1, CF:1
        or AF:1, af1:1, af2:1
    }?
    );
    stmts.append(&mut set_sub_carry_flag(&res, &a)?);
    stmts.append(&mut set_sub_overflow_flag(&res, &a, &b, sz)?);
    stmts.append(&mut set_parity_flag(&res)?);
    stmts.append(&mut write_reg(&a_, &res.clone().into(), sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn sub(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (a, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    let res = Variable::new2("res", None, sz)?;

    stmts.append(
        &mut rreil!{
        sub res:sz, (a), (b)
        cmplts SF:1, res:sz, [0]:sz
        cmpeq ZF:1, res:sz, [0]:sz
    }?
    );
    stmts.append(&mut set_sub_carry_flag(&res, &a)?);
    stmts.append(&mut set_sub_adj_flag(&res, &a)?);
    stmts.append(&mut set_sub_overflow_flag(&res, &a, &b, sz)?);
    stmts.append(&mut set_parity_flag(&res)?);
    stmts.append(&mut write_reg(&a_, &res.clone().into(), sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn xor(_a: Value, _b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (a, b, sz, mut stmts) = sign_extend(&_a, &_b)?;
    let res = Variable::new2("res", None, sz)?;

    if a == b {
        stmts.append(
            &mut rreil!{
            mov res:sz, [0]:sz
            mov SF:1, [0]:1
            mov ZF:1, [1]:1
            mov CF:1, [0]:1
            mov OF:1, [0]:1
            mov AF:1, ?
        }?
        );
    } else {
        stmts.append(
            &mut rreil!{
            xor res:sz, (a), (b)
            cmplts SF:1, res:sz, [0]:sz
            cmpeq ZF:1, res:sz, [0]:sz
            mov CF:1, [0]:1
            mov OF:1, [0]:1
            mov AF:1, ?
        }?
        );
    }

    stmts.append(&mut set_parity_flag(&res)?);
    stmts.append(&mut write_reg(&_a, &res.clone().into(), sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

//pub fn cmpxchg8b(_: Value) -> Result<(Vec<Statement>,JumpSpec)> { Ok((vec![],JumpSpec::FallThru)) }
pub fn cmpxchg16b(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cpuid() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn clc() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cld() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cli() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cmc() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn std() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sti() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn stc() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn cbw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cwd() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn clts() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

/*pub fn conv() -> Result<(Vec<Statement>,JumpSpec)> {
    /*let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"conv","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![],JumpSpec::FallThru))
}

pub fn conv2() -> Result<(Vec<Statement>,JumpSpec)> {
    /*
    let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"conv2","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true
    */
    Ok((vec![],JumpSpec::FallThru))
}*/

pub fn daa() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn das() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn dec(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn div(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn enter(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn hlt() -> Result<(Vec<Statement>, JumpSpec)> {
    /*let len = st.tokens.len();
    st.mnemonic(len,"hlt","",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    true*/
    Ok((vec![], JumpSpec::DeadEnd))
}

pub fn int3() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn int1() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn invd() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn idiv(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn imul1(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn imul2(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    // TODO: use this stmts or below? this seems dangerous...
    let (a, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    let res = Variable::new2("res", None, sz)?;
    //let mut stmts = vec![];
    // unused
    let _hsz = sz / 2;
    let dsz = sz * 2;
    let max = (1u64 << (sz - 1)) - 1;

    stmts.append(
        &mut rreil!{
        zext/dsz aa:dsz, (a)
        zext/dsz bb:dsz, (b)
        mul dres:dsz, aa:dsz, bb:dsz
        mov res:sz, dres:dsz
        cmplts SF:1, res:sz, [0]:sz
        mov ZF:1, ?
        mov AF:1, ?
        mov PF:1, ?
        cmpltu CF:1, [max]:dsz, dres:dsz
        mov OF:1, CF:1
    }?
    );
    stmts.append(&mut write_reg(&a_, &res.clone().into(), sz)?);

    Ok((stmts, JumpSpec::FallThru))

}

pub fn imul3(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn in_(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
//pub fn icebp() -> Result<(Vec<Statement>,JumpSpec)> { Ok((vec![],JumpSpec::FallThru)) }
pub fn inc(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn insb() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn insw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn int(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn into() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn iretw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::DeadEnd))
}

pub fn iret() -> Result<(Vec<Statement>, JumpSpec)> {
    /*let len = st.tokens.len();
    st.mnemonic(len,"iret","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    true*/
    Ok((vec![], JumpSpec::DeadEnd))
}

pub fn setcc(_: Value, _: Condition) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn seto(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::Overflow)
}
pub fn setno(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::NotOverflow)
}
pub fn setb(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::Below)
}
pub fn setae(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::AboveEqual)
}
pub fn setz(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::Equal)
}
pub fn setnz(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::NotEqual)
}
pub fn setbe(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::BelowEqual)
}
pub fn seta(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::Above)
}
pub fn sets(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::Sign)
}
pub fn setns(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::NotSign)
}
pub fn setp(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::Parity)
}
pub fn setnp(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::NotParity)
}
pub fn setl(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::Less)
}
pub fn setle(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::LessEqual)
}
pub fn setg(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::Greater)
}
pub fn setge(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    setcc(a, Condition::GreaterEqual)
}

pub fn cmovo(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::Overflow)
}
pub fn cmovno(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::NotOverflow)
}
pub fn cmovb(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::Below)
}
pub fn cmovae(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::AboveEqual)
}
pub fn cmovz(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::Equal)
}
pub fn cmovnz(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::NotEqual)
}
pub fn cmovbe(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::BelowEqual)
}
pub fn cmova(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::Above)
}
pub fn cmovs(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::Sign)
}
pub fn cmovns(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::NotSign)
}
pub fn cmovp(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::Parity)
}
pub fn cmovnp(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::NotParity)
}
pub fn cmovl(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::Less)
}
pub fn cmovle(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::LessEqual)
}
pub fn cmovg(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::Greater)
}
pub fn cmovge(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    cmovcc(a, b, Condition::GreaterEqual)
}


pub fn jmp(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::Jump(a)))
}

pub fn jcc(a: Value, cond: Condition) -> Result<(Vec<Statement>, JumpSpec)> {
    let (stmts,g) = match cond {
        Condition::Overflow => (vec![],Guard::Predicate{ flag: Variable::new2("OF", None, 1)?, expected: true }),
        Condition::NotOverflow => (vec![],Guard::Predicate{ flag: Variable::new2("OF", None, 1)?, expected: false }),
        Condition::Below => (vec![],Guard::Predicate{ flag: Variable::new2("CF", None, 1)?, expected: true }),
        Condition::AboveEqual => (vec![],Guard::Predicate{ flag: Variable::new2("CF", None, 1)?, expected: false }),
        Condition::Equal => (vec![],Guard::Predicate{ flag: Variable::new2("ZF", None, 1)?, expected: true }),
        Condition::NotEqual => (vec![],Guard::Predicate{ flag: Variable::new2("ZF", None, 1)?, expected: false }),
        Condition::BelowEqual => {
            let stmts = rreil!{
                or ZForCF:1, ZF:1, CF:1
            }?;
            (stmts,Guard::Predicate{ flag: Variable::new2("ZForCF", None, 1)?, expected: true })
        }
        Condition::Above => {
            let stmts = rreil!{
                cmpeq ZFnull:1, ZF:1, [0]:1
                cmpeq CFnull:1, CF:1, [0]:1
                and ZFandCFnull:1, ZFnull:1, CFnull:1
            }?;
            (stmts,Guard::Predicate{ flag: Variable::new2("ZFandCFnull", None, 1)?, expected: true })
        }
        Condition::Sign => (vec![],Guard::Predicate{ flag: Variable::new2("SF", None, 1)?, expected: true }),
        Condition::NotSign => (vec![],Guard::Predicate{ flag: Variable::new2("SF", None, 1)?, expected: false }),
        Condition::Parity => (vec![],Guard::Predicate{ flag: Variable::new2("PF", None, 1)?, expected: true }),
        Condition::NotParity => (vec![],Guard::Predicate{ flag: Variable::new2("PF", None, 1)?, expected: false }),
        Condition::Less => {
            let stmts = rreil!{
                xor SFxorOF:1, SF:1, OF:1
            }?;
            (stmts,Guard::Predicate{ flag: Variable::new2("SFxorOF", None, 1)?, expected: true })
        }
        Condition::LessEqual => {
            let stmts = rreil!{
                xor SFxorOF:1, SF:1, OF:1
                or ZForLess:1, ZF:1, SFxorOF:1
            }?;
            (stmts,Guard::Predicate{ flag: Variable::new2("ZForLess", None, 1)?, expected: true })
        }
        Condition::Greater => {
            let stmts = rreil!{
                cmpeq SFisOF:1, SF:1, OF:1
                cmpeq ZFnull:1, ZF:1, [0]:1
                and ZFandGreater:1, ZFnull:1, SFisOF:1
            }?;
            (stmts,Guard::Predicate{ flag: Variable::new2("ZFandGreater", None, 1)?, expected: true })
        }
        Condition::GreaterEqual => {
            let stmts = rreil!{
                cmpeq SFisOF:1, SF:1, OF:1
            }?;
            (stmts,Guard::Predicate{ flag: Variable::new2("SFisOF", None, 1)?, expected: true })
        }
    };
    Ok((stmts, JumpSpec::Branch(a, g)))
}

pub fn jo(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::Overflow)
}
pub fn jno(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::NotOverflow)
}
pub fn jb(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::Below)
}
pub fn jae(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::AboveEqual)
}
pub fn je(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::Equal)
}
pub fn jne(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::NotEqual)
}
pub fn jbe(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::BelowEqual)
}
pub fn ja(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::Above)
}
pub fn js(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::Sign)
}
pub fn jns(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::NotSign)
}
pub fn jp(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::Parity)
}
pub fn jnp(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::NotParity)
}
pub fn jl(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::Less)
}
pub fn jle(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::LessEqual)
}
pub fn jg(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::Greater)
}
pub fn jge(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    jcc(a, Condition::GreaterEqual)
}

pub fn jrcxz(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
//pub fn jecxz(_: Value) -> Result<(Vec<Statement>,JumpSpec)> { Ok((vec![],JumpSpec::FallThru)) }
//pub fn jrcxz(_: Value) -> Result<(Vec<Statement>,JumpSpec)> { Ok((vec![],JumpSpec::FallThru)) }

pub fn lahf() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn sahf() -> Result<(Vec<Statement>, JumpSpec)> {
    let stmts = rreil!{
        mov CF:1, AH:1
        sel/2/1 PF:1, AH:8
        sel/4/1 AF:1, AH:8
        sel/6/1 ZF:1, AH:8
        sel/7/1 SF:1, AH:8
    }?;

    Ok((stmts, JumpSpec::FallThru))
}

pub fn lsl(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lar(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lds(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    lxs(a, b, Variable::new2("DS", None, 16)?.into())
}
pub fn les(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    lxs(a, b, Variable::new2("ES", None, 16)?.into())
}
pub fn lss(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    lxs(a, b, Variable::new2("SS", None, 16)?.into())
}
pub fn lfs(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    lxs(a, b, Variable::new2("FS", None, 16)?.into())
}
pub fn lgs(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    lxs(a, b, Variable::new2("GS", None, 16)?.into())
}
pub fn lxs(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lea(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (_, b, sz, mut stmts) = zero_extend(&a, &b)?;

    stmts.append(&mut write_reg(&a, &b, sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn leave() -> Result<(Vec<Statement>, JumpSpec)> {
    /*
    let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"leave","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![], JumpSpec::FallThru))
}

pub fn lodsw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn lodsb() -> Result<(Vec<Statement>, JumpSpec)> {
    /*
    let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"lodsb","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![], JumpSpec::FallThru))
}

/*pub fn lods() -> Result<(Vec<Statement>,JumpSpec)> {
    /*let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"lods","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![],JumpSpec::FallThru))
}*/

pub fn loop_(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    /*
    let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"loop","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![], JumpSpec::FallThru))
}

pub fn loope(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    /*
    let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"loope","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![], JumpSpec::FallThru))
}

pub fn loopne(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    /*
    let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"loopne","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![], JumpSpec::FallThru))
}

pub fn mov(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (_, b, sz, mut stmts) = zero_extend(&a_, &b_)?;

    stmts.append(&mut write_reg(&a_, &b, sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn movbe(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn movsb() -> Result<(Vec<Statement>, JumpSpec)> {
    /*
    let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"movsb","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![], JumpSpec::FallThru))
}

pub fn movsw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

/*pub fn movs() -> Result<(Vec<Statement>,JumpSpec)> {
    /*let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"movs","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![],JumpSpec::FallThru))
}*/

pub fn movsx(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (_, b, sz, mut stmts) = sign_extend(&a_, &b_)?;

    stmts.append(&mut write_reg(&a_, &b, sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn movzx(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (_, b, sz, mut stmts) = zero_extend(&a_, &b_)?;

    stmts.append(&mut write_reg(&a_, &b, sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn mul(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn neg(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn nop(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lock() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rep() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn repne() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn not(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn out(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn outsb() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn outsw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn popfw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pushfw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pop(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let opsz = 64;
    let addrsz = 64;
    let mut stmts = vec![];
    let res = Variable::new2("res",None,opsz)?;

    if let Some(sz) = a.bits() {
        if sz < opsz {
            stmts = rreil!{
                    zext/opsz (res), (a)
            }?;
        } else if sz > opsz {
            stmts = rreil!{
                    sel/0/opsz (res), (a)
            }?;
        }
    }

    let a = res;
    let sp = match addrsz {
        16 => Variable::new2("SP",None,16)?,
        32 => Variable::new2("ESP",None,32)?,
        64 => Variable::new2("RSP",None,64)?,
        _ => return Err(format!("Invalid AddressSize: {}",addrsz).into()),
    };

    let bytes = opsz / 8;
    stmts.append(&mut rreil!{
        mov stack:addrsz, (sp)
        load/RAM/le/opsz val:opsz, stack:addrsz
        add stack:addrsz, (sp), [bytes]:addrsz
    }?);

    stmts.append(&mut write_reg(&a.into(), &Variable::new2("val",None,addrsz)?.into(),opsz)?);
    stmts.append(&mut write_reg(&sp.into(), &Variable::new2("stack",None,addrsz)?.into(),addrsz)?);

    Ok((stmts,JumpSpec::FallThru))
}

pub fn popa() -> Result<(Vec<Statement>, JumpSpec)> {
    /*
    let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"popa","",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![], JumpSpec::FallThru))
}

pub fn popcnt(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
//pub fn popf(_: Value) -> Result<(Vec<Statement>,JumpSpec)> { Ok((vec![],JumpSpec::FallThru)) }

pub fn push(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let opsz = 64;
    let addrsz = 64;
    let mut stmts = vec![];
    let res = Variable::new2("res",None,opsz)?;

    if let Some(sz) = a.bits() {
        if sz < opsz {
            stmts = rreil!{
                    zext/opsz (res), (a)
            }?;
        } else if sz > opsz {
            stmts = rreil!{
                    sel/0/opsz (res), (a)
            }?;
        }
    }

    let bytes = a.bits().unwrap_or(32) / 8;
    let a = res;
    let sp = match addrsz {
        16 => Variable::new2("SP",None,16)?,
        32 => Variable::new2("ESP",None,32)?,
        64 => Variable::new2("RSP",None,64)?,
        _ => return Err(format!("Invalid AddressSize: {}",addrsz).into()),
    };

    stmts.append(&mut rreil!{
            sub stack:addrsz, (sp), [bytes]:addrsz
            store/RAM/le/opsz (a), stack:addrsz
    }?);

    stmts.append(&mut write_reg(&sp.into(), &Variable::new2("stack", None, addrsz)?.into(),addrsz)?);

    Ok((stmts,JumpSpec::FallThru))
}

pub fn pusha() -> Result<(Vec<Statement>, JumpSpec)> {
    /*let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"pusha","",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![], JumpSpec::FallThru))
}

//pub fn pushf(_: Value) -> Result<(Vec<Statement>,JumpSpec)> { Ok((vec![],JumpSpec::FallThru)) }
pub fn rcl(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rcr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn ret() -> Result<(Vec<Statement>, JumpSpec)> {
    let stmts = rreil!{
            ret
    }?;
    Ok((stmts, JumpSpec::DeadEnd))
}

pub fn retn(a: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let opsz = 64;
    let addrsz = 64;
    let mut stmts = vec![];
    let res = Variable::new2("res",None,opsz)?;

    if let Some(sz) = a.bits() {
        if sz < opsz {
            stmts = rreil!{
                    zext/opsz (res), (a)
            }?;
        } else if sz > opsz {
            stmts = rreil!{
                    sel/0/opsz (res), (a)
            }?;
        }
    }

    let a = res;
    let sp = match addrsz {
        16 => Variable::new2("SP",None,16)?,
        32 => Variable::new2("ESP",None,32)?,
        64 => Variable::new2("RSP",None,64)?,
        _ => return Err(format!("Invalid AddressSize: {}",addrsz).into()),
    };

    stmts.append(&mut rreil!{
            add stack:addrsz, (sp), (a)
    }?);

    stmts.append(&mut write_reg(&sp.into(), &Variable::new2("stack", None, addrsz)?.into(),addrsz)?);

    Ok((stmts, JumpSpec::DeadEnd))
}

pub fn retf() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::DeadEnd))
}

pub fn retnf(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::DeadEnd))
}

pub fn ror(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rol(_a: Value, _b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    /*    let (a,b,sz,mut stmts) = try!(sign_extend(&a_,&b_));
    let res = Variable::new2("res", None, sz)?;
    let mut stmts = vec![];
    let msb = sz - 1;

    stmts.append(&mut try!(rreil!{
        mov msb:1, (a.extract(1,sz - 1).unwrap());
        zext/dsz bb:dsz, (b);
        mul dres:dsz, aa:dsz, bb:dsz;
        mov res:sz, dres:dsz;
        cmplts SF:1, res:sz, [0]:sz;
        mov ZF:1, ?;
        mov AF:1, ?;
        mov PF:1, ?;
        cmpltu CF:1, [max]:dsz, dres:dsz;
        mov OF:1, CF:1;
    }));
    stmts.append(&mut try!(write_reg(&a_,&res.clone().into(),sz)));

    Ok((stmts,JumpSpec::FallThru))
*/
    Ok((vec![], JumpSpec::FallThru))
}

pub fn sal(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn salc() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sar(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn scasw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn scasb() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn scas() -> Result<(Vec<Statement>, JumpSpec)> {
    /*let next = st.address + (st.tokens.len() as u64);
    let len = st.tokens.len();

    st.mnemonic(len,"scas","{u}",Ok((vec![],JumpSpec::FallThru)),&|| {} );
    st.jump(Value::new_u64(next),Guard::always());
    true*/
    Ok((vec![], JumpSpec::FallThru))
}

pub fn shl(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn shr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn stosb() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn stosw() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn test(a: Value, b: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (a, b, sz, mut stmts) = sign_extend(&a, &b)?;
    let res = Variable::new2("res", None, sz)?;
    let mut s = rreil!{
        and res:sz, (a), (b)
        cmplts SF:1, res:sz, [0]:sz
        cmpeq ZF:1, res:sz, [0]:sz
    }?;

    stmts.append(&mut s);
    stmts.append(&mut set_parity_flag(&res)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn ud1() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ud2() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xadd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xchg(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rdtsc() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xgetbv(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xlatb() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn wait() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn wbinvd() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn prefetch(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn movsxd(a_: Value, b_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    let (_, b, sz, mut stmts) = sign_extend(&a_, &b_)?;
    stmts.append(&mut write_reg(&a_, &b, sz)?);

    Ok((stmts, JumpSpec::FallThru))
}

pub fn syscall() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sysret() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn movapd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn wrmsr() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rdmsr() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rdpmc() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sysenter() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sysexit() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn getsec() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmread(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmwrite(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
//pub fn vmenter() -> Result<(Vec<Statement>,JumpSpec)> { Ok((vec![],JumpSpec::FallThru)) }
//pub fn vmexit() -> Result<(Vec<Statement>,JumpSpec)> { Ok((vec![],JumpSpec::FallThru)) }

pub fn montmul() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xcryptecb() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rsm() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmaddwd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn tzcnt(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lzcnt(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn crc32(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn vmcall() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmlaunch() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmresume() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmxoff() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn clac() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn stac() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn encls() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xsetbv() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmfunc() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xend() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xtest() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn enclu() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn swapgs() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rdtscp() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rdrand(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rdseed(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rdpid(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cmpxch8b(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmptrld(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmptrst(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmclear(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmxon(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xabort(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xbegin(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rdfsbase(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rdgsbase(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn wrfsbase(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn wrgsbase(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fxsave() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fxstor() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xsave() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xrstor() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xsaveopt() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn clflush() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn blsr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn blsmsk(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn blsi(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn andn(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn bextr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn blendd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn blendvb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn bzhi(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn clgi() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtph2ps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn extracti128(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fist(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fldl2g() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fperm() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fperm1() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn frndintx() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fsubrp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn gatherdd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn gatherdps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn gatherqd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn gatherqps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn inserti128(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn invept(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn invlpg(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn invpcid(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn invvpid(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lgdt(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lidt(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lldt(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lmsw(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ltr(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn maskmovpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn maskmovps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pavgusb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pblendw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pclmulqdq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpgtq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn perm2f128(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn perm2i128(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn permd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn permilp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn permilpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn permilps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn permpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn permq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pf2id(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pf2iw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfacc(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfadd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfcmpeq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfcmpge(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfcmpgt(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfmax(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfmin(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfmul(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfnacc(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfpnacc(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfrcp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfrcpit1(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfrcpit2(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfrsqit1(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfrsqrt(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfsub(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pfsubr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn phminposuw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pi2fd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pi2fw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmaddubsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmaskmovd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovsxbd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovsxbq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovsxbw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovsxdq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovsxwd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovsxwq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovzxbd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovzxbq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovzxbw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovzxdq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovzxwd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovzxwq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmulhrsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmulhrw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psignb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psignd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psignw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pswapd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn punpckldq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sgdt(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sha1msg1(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sha1msg2(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sha1nexte(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sha1rnds4(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sha256msg1(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sha256msg2(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sha256rnds2(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn shlx(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sidt(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sldt(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn smsw(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn str(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn testpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn testps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn verr(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn verw(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// MMX
pub fn emms() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn packsswb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn packssdw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn packuswb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn paddb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn paddw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn paddd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn paddsb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn paddsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn paddusb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn paddusw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pand(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pandn(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpeqb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpeqw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpeqd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpgtb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpgtw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpgtd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmadwd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmulhw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmullw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn por(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psraw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psrad(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psrlw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psrld(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psrlq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psllw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pslld(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psllq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psubb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psubw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psubd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psubsb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psubsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psubusb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psubusw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn punpckhbw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn punpckhwd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn punpckhdq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn punpcklbw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn punpcklwd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn punpcklqdq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pxor(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// SSE 1
pub fn addps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn addss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn andnps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn andps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cmpps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cmpss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn comiss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtpi2ps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtps2pi(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtsi2ss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtss2si(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvttps2pi(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvttss2si(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn divps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn divss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ldmxcsr() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn maskmovq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn maxps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn maxss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn minps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn minss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movaps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn minhps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movlps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movmskps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movntps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movntq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movups(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn mulps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn mulss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn orps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pavgb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pavgw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pextrw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pinsrw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmaxsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmaxub(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pminsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pminub(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovmskb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmulhuw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn prefetchnta(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn prefetcht0(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn prefetcht1(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn prefetcht2(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn prefetchw(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn prefetchwt1(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psadbw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pshufw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pshufb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rcpps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rcpss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rsqrtps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn rsqrtss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sfence() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn shufps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sqrtps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sqrtss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn stmxcsr() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn subps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn subss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ucomiss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn unpckhps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn unpcklps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xorps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// SSE 2
pub fn addpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn addsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn andnpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn andpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cflush(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cmppd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cmpsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn comisd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtdq2pd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtdq2ps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtpd2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtpd2pi(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtpd2ps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtpi2pd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtps2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtps2pd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtsd2si(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtsd2ss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtsi2sd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvtss2sd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvttpd2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvttpd2pi(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvttps2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn cvttsd2si(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn divpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn divsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lfence() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn maskmovdqu(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn maxpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn maxsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn mfence() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn minpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn minsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movdq2q(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movdaq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movdqa(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movdqu(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movhpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movhps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movlpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movmskpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movntdq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movntdqa(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movnti(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movntpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movq2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movupd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn mulpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn mulsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn orpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pabsb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pabsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pabsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn paddq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pause() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmuludq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pshufd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pshufhw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pshuflw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pslldq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psarw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psrldq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn psubq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pusbsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn punckhwd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn punpckhqdq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn puncklqdq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn puncklwd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn shufpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sqrtpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn sqrtsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn subpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn subsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ucomisd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn unpckhpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn unpcklpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn xorpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// SSE 4
pub fn blendpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn blendps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn blendvpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn blendvps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn dppd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn dpps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn extractps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn insertps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn mpsadbw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pblendbw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpestri(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpestrm(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpistri(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpistrm(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pextrb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pextrd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pextrq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pinsrb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pinsrd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pinsrq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn roundpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn roundps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn roundsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn roundss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovsx(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmovzx(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pminsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pminsb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pminud(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pminuw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmaxsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmaxsb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmaxud(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmaxuw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ptest(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmulld(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pmuldq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn phaddw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn phaddsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn phaddd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn phsubw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn phsubsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn phsubd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn packusdw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pblendvb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pcmpeqq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn phminpushuw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// SSE 3
pub fn addsubpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn addsubps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn haddpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn haddps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn hsubpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn hsubps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn lddqu(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn monitor() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movddup(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movshdup(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn movsldup(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn mwait() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn palignr(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// AVX
pub fn aesdec(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmovd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn aesdeclast(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn aesenc(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn aesenclast(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn aesimc(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn aeskeygenassist(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vboradcastss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vboradcastsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vboradcastf128(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vzeroupper() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// FPU
pub fn f2xm1() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fabs() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fadd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn faddp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fiadd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fbld(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fbstp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fchs() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fclex() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnclex(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcmovb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcmove(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcmovbe(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcmovu(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcmovnb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcmovne(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcmovnbe(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcmovnu(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcom(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcomp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcompp() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcomi(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcomip(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fucomi(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fucomip(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fcos() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fdecstp() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fdiv(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fdivp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fidiv(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fdivr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fdivrp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fidivr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ffree(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ficom(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ficomp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fild(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fincstp() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn finit() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fninit(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fistp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fisttp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fld(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fld1() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fldl2t() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fldl2e() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fldpi() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fldlg2() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fldln2() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fldz() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fldcw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmul(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmulp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fimul(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnop() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fpatan() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fprem() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fprem1() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fptan() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn frndint() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn frstor(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fsave(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnsave(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fscale() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fsin() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fsincos() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fsqrt() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fst1(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fst2(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fstp(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fstcw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fldenv(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fstenv(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnstenv(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fstsw1(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fstsw2(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnstsw(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fsub(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fsubp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fisub(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fsubr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fisubr(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn ftst() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fucom(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fucomp(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fucompp() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fxam() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fxch(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fxtract() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fyl2x() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fyl2xp1() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// MPX
pub fn bndcl(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn bndcu(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn bndcn(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn bndmov(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn bndmk(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn bndldx(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn bndstx(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn noop() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn noop_unary(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn noop_binary(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// FMA
pub fn fmadd132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmadd132ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmadd213ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmadd213ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmadd231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmadd231ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmaddsub132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmaddsub231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmaddsub232ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmnadd132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmnsub132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmsub132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmsub132ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmsub213ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmsub213ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmsub231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmsub231ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmsubadd132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmsubadd231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fmsubadd232ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnmadd213ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnmadd213ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnmadd231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnmadd231ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnmsub213ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnmsub213ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnmsub231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fnmsub231ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

// AVX
pub fn vaddpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaddps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaddsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaddss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaddsubpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaddsubps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaesdec(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaesdeclast(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaesenc(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaesenclast(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaesimc(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vaeskeygenassist(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vandpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vandps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vandnpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vandnps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vblendpd(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vblendps(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vblendvpd(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vblendvps(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcmppd(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcmpps(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcmpsd(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcmpss(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcomisd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcomiss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtdq2pd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtdq2ps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtpd2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtpd2ps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtps2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtps2pd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtsd2si(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtsd2ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtsi2sd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtss2sd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtsi2ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvttpd2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvttps2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvttsd2si(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvttss2si(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vdivps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vdivpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vdivss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vdivsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vdppd(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vdpps(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vextractps(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vhaddpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vhaddps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vhsubpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vhsubps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vinsertps(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vlddqu(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vldmxcsr(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmaxpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmaxsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmaxps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmaxss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vminpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vminsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vminps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vminss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmovhpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmovhps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmovlpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmovlps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmovsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmovss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmpsadbw(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vorpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vorps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpabsb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpabsw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpabsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpacksswb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpackssdw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpackusdw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpackuswb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpaddb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpaddw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpaddd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpaddq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpaddsb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpaddsw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpaddusb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpaddusw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpalignr(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpand(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpandn(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpavgb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpavgw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpblendvb(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpblendw(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpclmulqdq(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpeqb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpeqw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpeqd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpeqq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpgtb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpgtw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpgtd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpgtq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vphaddw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vphaddd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vphaddsw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vphminposuw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vphsubw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vphsubd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vphsubsw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpinsrb(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpinsrd(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpinsrw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaddubsw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmadwd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaxsb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaxsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaxsw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaxub(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaxud(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaxuw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpminsb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpminsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpminsw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpminub(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpminud(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpminuw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmuldq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmulhrsw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmulhuw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmulhw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmulld(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmullw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmuludq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpor(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsadbw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsignb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsignw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsignd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpslldq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsllw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpslld(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsllq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsrad(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsarw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsrldq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsrlw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsrld(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsrlq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsubb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsubw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsubd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsubq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsubsb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpusbsw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsubusb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsubusw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vptest() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpunpckhbw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpunckhwd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpunpckhdq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpunpckhqdq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpunpcklbw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpunpckldq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpuncklqdq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpuncklwd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpxor(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vrcpps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vroundpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vroundps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vroundsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vroundss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vrsqrtps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vrsqrtss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vsqrtss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vsqrtsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vshufps(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vshufpd(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vsubps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vsubss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vsubpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vsubsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vunpckhps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vunpcklps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vunpckhpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vunpcklpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vbroadcastss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vbroadcastsd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vbroadcastf128(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vextractf128(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vextracti128(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vgatherdd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vgatherdp(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vgatherpdp(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vgatherqpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vinsertf128(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vinserti128(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmaskmovps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmaskmovpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmulps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmulss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmulpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmulsd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vblendd(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpboradcastb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpboradcastw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpboradcastd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpboradcastq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpboradcasti128(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpermd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpermpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpermps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpermq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vperm2i128(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpermilpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpermilps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vperm2f128(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaskmovd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaskmovq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsllvd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsravd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsrlvd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vtestpd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vtestps(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vzeroall() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vxorps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vxorpd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn broadcastf128(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn broadcasti128(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn broadcastsd(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn broadcastss(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fst(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fstp1(_: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn fstp2(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pboradcastw(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pbroadcastb(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pbroadcastd(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn pbroadcastq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vandn(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vbextr(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vblendvb(_: Value, _: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vbzhi(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vcvtph2ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmadd132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmadd132ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmadd213ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmadd213ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmadd231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmadd231ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmaddsub132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmaddsub231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmaddsub232ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmnadd132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmnsub132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmsub132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmsub132ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmsub213ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmsub213ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmsub231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmsub231ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmsubadd132ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmsubadd231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfmsubadd232ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfnmadd213ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfnmadd213ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfnmadd231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfnmadd231ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfnmsub213ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfnmsub213ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfnmsub231ps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vfnmsub231ss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vgatherdps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vgatherqd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vgatherqps(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vmovq2dq(_: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpestri(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpestrm(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpistri(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpcmpistrm(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpermilp(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpextrw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpmaddwd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpshufb(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpshufd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpshufhw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpshuflw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpshufw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsraw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpsubsw(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpunpckhwd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpunpcklqdq(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vpunpcklwd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vrcpss(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vsha1rnds4(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vshld(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vshlx(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
pub fn vshrd(_: Value, _: Value, _: Value) -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn endbr64() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}

pub fn endbr32() -> Result<(Vec<Statement>, JumpSpec)> {
    Ok((vec![], JumpSpec::FallThru))
}
