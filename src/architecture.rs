/*
 * Panopticon - A libre disassembler
 * Copyright (C) 2015, 2017  Panopticon authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use p8n_types::{
    Architecture,
    Region,
    Match,
    Result,
};

#[derive(Clone,Debug)]
pub enum Amd64 {}

#[derive(Clone,PartialEq,Copy,Debug)]
pub enum Mode {
    Real, // Real mode / Virtual 8086 mode
    Protected, // Protected mode / Long compatibility mode
    Long, // Long 64-bit mode
}

impl Mode {
    pub fn alt_bits(&self) -> usize {
        match self {
            &Mode::Real => 32,
            &Mode::Protected => 16,
            &Mode::Long => 16,
        }
    }

    pub fn bits(&self) -> usize {
        match self {
            &Mode::Real => 16,
            &Mode::Protected => 32,
            &Mode::Long => 64,
        }
    }
}

impl Architecture for Amd64 {
    type Configuration = Mode;

    fn prepare(_: &Region, _: &Self::Configuration) -> Result<Cow<'static, [(&'static str, u64, &'static str)]>> {
        Ok(Cow::Owned(vec![]))
    }

    fn decode(reg: &Region, start: u64, cfg: &Mode, matches: &mut Vec<Match>) -> Result<()> {
        let mut buf: Vec<u8> = vec![0u8; 15];
        let p = start;

        trace!("read from {:#x}",start);
        let len = reg.try_read(start,&mut buf[..])?;

        trace!("disass @ {:#x}: {:?}", p, buf);
        let ret = ::disassembler::read(*cfg, &buf[0..len], p)?;
        trace!("    res: {:?}", ret);

        for m in matches.iter() {
            for stmt in m.instructions.iter() {
                stmt.sanity_check().unwrap();
            }
        }

        matches.push(ret);
        Ok(())
    }
}
