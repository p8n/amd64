/*
 * Panopticon - A libre disassembler
 * Copyright (C) 2015, 2017  Panopticon authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

extern crate p8n_types;
extern crate p8n_amd64;
extern crate rand;
extern crate capstone;

use p8n_amd64 as amd64;
use p8n_types as types;
use capstone::prelude::*;
use capstone::arch;
use rand::{thread_rng, Rng};

#[test]
fn fuzz_decoder() {
    let mut cs = Capstone::new()
        .x86()
        .mode(arch::x86::ArchMode::Mode64)
        .build().unwrap();
    let mut buf = [0u8; 15];
    let mut rng = thread_rng();
    let mut i = 0u64;

    println!("Fuzz testing against Capstone. Press Ctrl+C to stop.");

    loop {
        // sample random (0..15] byte vector.
        let len: usize = rng.gen_range(1, 16);
        rng.fill(&mut buf[0..len]);

        let cs_res = cs.disasm_count(&buf[0..len], 0, 1).unwrap();
        let cs_ins = cs_res.iter().collect::<Vec<_>>();
        let my_res = amd64::read(amd64::Mode::Long, &buf[0..len], 0).ok();

        if cs_ins.len() != 1 {
            if my_res.is_some() {
                println!("Error after {} samples.", i);
                println!("buf: {:?}", &buf[0..len]);
                println!("cs: {:?}", cs_res);
                println!("my: {:?}", my_res);
                unreachable!()
            }
        } else {
            let cs_op = cs_ins[0].mnemonic().map(|x| x.to_string());

            if my_res.clone().map(|x| x.opcode.to_string()) != cs_op {
                println!("Error after {} samples.", i);
                println!("buf: {:?}", buf);
                println!("cs: {:?}", cs_res);
                println!("cs opcode: {:?}", cs_op);
                println!("my: {:?}", my_res);
                unreachable!()
            }
        }

        i += 1;
    }
}
